name := """play-java-intro"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)
lazy val myProject = (project in file(".")).enablePlugins(PlayJava, PlayEbean)
scalaVersion := "2.11.8"

libraryDependencies += javaJpa

libraryDependencies += javaJdbc

libraryDependencies += evolutions

lazy val nonEnhancedProject = (project in file("non-enhanced"))
  .disablePlugins(PlayEnhancer)

libraryDependencies += "org.mockito" % "mockito-core" % "2.1.0"

libraryDependencies += javaWs % "test"

libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.36"

libraryDependencies += "org.hibernate" % "hibernate-core" % "5.2.5.Final"

lazy val Resturant = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"

libraryDependencies ++= Seq(
  "com.adrianhurt" %% "play-bootstrap" % "1.1-P25-B3"
)