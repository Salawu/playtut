package models;


import javax.persistence.Entity;
import javax.persistence.Id;
import com.avaje.ebean.Model;
import static play.data.validation.Constraints.*;


/**
 * Created by i.Sec on 4/6/2017.
 */
@Entity
public class Contact extends Model {

    @Id
    Long id;

    @Required
    String name;

    @Required
    @Email
    String address;


    public static Finder<Long, Contact> find =
            new Finder<Long, Contact>(Long.class, Contact.class);



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


}
