package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Transaction;
import models.Contact;


import play.api.data.*;
import play.data.Form;
import play.api.mvc.Request;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import scala.Int;
import views.html.*;


import javax.inject.Inject;
import javax.persistence.PersistenceException;
import java.util.Arrays;
import java.util.List;


/**
 * Created by i.Sec on 4/6/2017.
 */
public class OrderController extends Controller {

    private FormFactory formFactory;



  //  static Form<Contact> contactForm = new Form(Contact.class);

    @Inject
    public OrderController(FormFactory formFactory) {
        this.formFactory = formFactory;
    }


    public Result index(){


        play.data.Form<Contact> contactForm = formFactory.form(Contact.class);
        List<Contact> contacts = Contact.find.all();

        return ok(index.render(contacts, contactForm));

    }

    public Result another(){

      if (!session().containsKey("counter")){

          session("counter", "0");
      }
      String currentStringValue = session("counter");

        Integer  currentValue = Integer.parseInt(currentStringValue);

        Integer newValue = currentValue + 1;

        session("counter", newValue.toString());

        return ok(another.render());
    }

    public Result create(){

       // Form<Contact> form = contactForm.bindFromRequest((Request<?>) request());

        Form<Contact> form= formFactory.form(Contact.class).bindFromRequest();

           if(!form.hasErrors()){

            form.get().save();

            return redirect(routes.OrderController.index());
        }else{

           return badRequest(index.render(Contact.find.all(), form));
        }
    }

    public Result edit(long id){

        play.data.Form<Contact> contactForm = formFactory.form(Contact.class);

        Contact contact = Contact.find.byId(id);
        if(contact == null){
            return redirect(routes.OrderController.index());

        }else{

            Form<Contact> form = contactForm.fill(contact);
            return ok(edit.render(id, form));
        }
    }

    public Result update(Long id)throws PersistenceException{

        play.data.Form<Contact> contactForm = formFactory.form(Contact.class);

        Form<Contact> form = contactForm.bindFromRequest(request());

        if(!form.hasErrors()){

            form.get().update(id);

            return redirect(routes.OrderController.index());

        }else{

            return badRequest(edit.render(id, form));
        }
    }


    public Result delete(long id){


        return TODO;
    }
}
